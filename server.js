/* var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors')
var router = require('./app/router/router.js');
var app = express();
const helmet = require('helmet')
    // log every request to the console
app.use(cors());
app.use(bodyParser.urlencoded({ limit: '500mb', extended: true })) // parse application/x-www-form-urlencoded
app.use(bodyParser.json({ limit: '500mb', extended: true })) // parse application/json
app.use(bodyParser.json());
// Router
app.use('/api_router', router);
app.use(function(req, res, next) { //remove cors security for local host
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
});
// app.use(function(err, req, res, next) {
//     console.error(err.stack)
//     res.status(500).send('Something broke!' + '===' + err.stack)
// })
app.use(helmet.contentSecurityPolicy({
        directives: {
            defaultSrc: ["'self'"],
            styleSrc: ["'self'", 'maxcdn.bootstrapcdn.com']
        }
    }))
    // Rabbit connection intialize
app.get('/', function(req, res) {
    // res.send('express running on lambda');
    res.redirect('http://buzzable-frontend.s3-website.ap-south-1.amazonaws.com/');
})

app.get('/buzzable_admin', function(req, res) {
    res.redirect('http://buzzable-backend.s3-website.ap-south-1.amazonaws.com/');
})
module.exports = app;
*/


var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var router = require('./app/router/router.js');
var app = express();

                                        // log every request to the console
app.use(cors());
app.use(bodyParser.urlencoded({limit: '500mb', extended: true})) // parse application/x-www-form-urlencoded
app.use(bodyParser.json({limit: '500mb', extended: true}))       // parse application/json
app.use(bodyParser.json());
// Router
app.use('/', router);

// Rabbit connection intialize
 
 var server = app.listen(8080, function () {
 var host = server.address().address;
 var port = server.address().port;
 console.log("Example app listening at http://" + host + ':' + port);
})

 module.exports = app;









