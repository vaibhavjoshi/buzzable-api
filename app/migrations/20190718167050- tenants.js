'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('tenants', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      }, 
      account_name: {   
        type: Sequelize.CHAR
      },
      contact_email: {   
        type: Sequelize.CHAR
      },
      contact_phone: {   
        type: Sequelize.CHAR
      },
      contact_city: {   
        type: Sequelize.CHAR
      },
      contact_state: {   
        type: Sequelize.CHAR
      },
      conatct_zip: {   
        type: Sequelize.CHAR
      },
      contact_country: {   
        type: Sequelize.CHAR
      },
      created_at: {
			type: Sequelize.DATE(),
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
		},
	  updated_at: {
			type: Sequelize.DATE(),
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        },
    active_status: {
      type: Sequelize.BOOLEAN
      },    
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable(' tenants');
  }
};

