'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('questions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      question: {
        type: Sequelize.TEXT
      },
      created_at: {
			type: Sequelize.DATE(),
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
		},
	  updated_at: {
			type: Sequelize.DATE(),
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        },
    deleted_at: {
      type: Sequelize.BIGINT
      },    
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable(' questions');
  }
};

