'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('media_item', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      response_media_id: {
        type: Sequelize.BIGINT
      },
      media_type: {
        type: Sequelize.CHAR
      },
      media_path: {
        type: Sequelize.CHAR
      },
      media_description: {
        type: Sequelize.CHAR
      },
      media_size: {
        type: Sequelize.DATE
      },
      created_at: {
			type: Sequelize.DATE(),
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
		},
	  updated_at: {
			type: Sequelize.DATE(),
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        },
    deleted_at: {
      type: Sequelize.BIGINT
      },    
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable(' media_item');
  }
};

