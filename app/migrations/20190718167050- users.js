'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('users', {
      tenant_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      }, 
      username: {   
        type: Sequelize.CHAR
      },
      password	: {   
        type: Sequelize.CHAR
      },
      user_type: {   
        type: Sequelize.CHAR
      },
      email_id: {   
        type: Sequelize.CHAR
      },
      first_name: {   
        type: Sequelize.CHAR
      },
      last_name: {   
        type: Sequelize.CHAR
      },
      last_login: {   
        type: Sequelize.DATE()
      },
      created_at: {
			type: Sequelize.DATE(),
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
		},
	  updated_at: {
			type: Sequelize.DATE(),
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        },
    active_status: {
      type: Sequelize.BOOLEAN
      },    
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('users');
  }
};

