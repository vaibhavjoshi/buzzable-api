'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('matter', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER
      },
      tenant_id: {
        type: Sequelize.INTEGER
      },
      matter_name: {
        type: Sequelize.CHAR
      },
      matter_description: {
        type: Sequelize.TEXT
      },
      matter_category: {
        type: Sequelize.CHAR
      },
      matter_url: {
        type: Sequelize.CHAR
      },
      created_at: {
			type: Sequelize.DATE(),
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
		},
	  updated_at: {
			type: Sequelize.DATE(),
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        },
      expiry_date: {
            type: Sequelize.DATE
        },
      archived: {
            type: Sequelize.BOOLEAN
          },    
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('roles');
  }
};

