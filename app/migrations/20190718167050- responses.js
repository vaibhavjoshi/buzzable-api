'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('responses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      matter_id: {   
        type: Sequelize.BIGINT
      },
      device_id: {   
        type: Sequelize.CHAR
      },
      email_id: {   
        type: Sequelize.CHAR
      },
      submitted_at: {   
        type: Sequelize.DATE()
      },
      created_at: {
			type: Sequelize.DATE(),
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
		},
	  updated_at: {
			type: Sequelize.DATE(),
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        },
    deleted_at: {
      type: Sequelize.BIGINT
      },    
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable(' responses');
  }
};

