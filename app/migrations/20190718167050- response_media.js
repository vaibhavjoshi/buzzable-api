'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('response_media', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      response_id: {
        type: Sequelize.BIGINT
      },
      response_sentiment: {
        type: Sequelize.CHAR
      },
      response_category: {
        type: Sequelize.CHAR
      },
      response_relevance: {
        type: Sequelize.CHAR
      },
      response_review_required: {
        type: Sequelize.BOOLEAN
      },
      created_at: {
			type: Sequelize.DATE(),
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
		},
	   updated_at: {
			type: Sequelize.DATE(),
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        },
    deleted_at: {
      type: Sequelize.BIGINT
      },    
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('response_media');
  }
};

