//var libs = process.cwd() + '/libs/';

module.exports = function() {
    this.notFound = function(res) {
        res.json({ 
            response:{
                status:'404',
                msg : 'Not Found',
                data:[{}],
                error:['URL not found']
            }
             
        });
    };

    this.errorHandlers = function(res,err_msg) {
        res.json({ 
            response:{
                status:'500',
                msg : 'Internal server error',
                data:[{}],
                error:[err_msg]
            }
             
        });
    };
};