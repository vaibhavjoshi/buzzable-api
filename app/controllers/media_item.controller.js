"use strict";

var models = require('../models/index');

module.exports = {
    // POST Discription
    POST: function(req, res) {
        const discriptionData = req.body
        console.log("discriptionData ",discriptionData)
        models.Media_item.create({
                media_description: discriptionData.responseMedia,
                response_media_id: discriptionData.response_media_id,
                media_type : discriptionData.media_type
            })
            .then(function(result) {
                res.json({ data: result, status: 200, textData: 'data' })
            })
    }
}