const multer = require('multer');
const fs = require('fs');
var express = require('express');
var router = express.Router();
const Pool = require('pg').Pool
const pool = new Pool({
  user: 'postgres',
  host: '34.93.24.169',
  database: 'buzzableDB',
  password: '$buzz@123',
  port: 5432,
})
const {
    Storage
} = require('@google-cloud/storage');
var path = require('path')
const gcs = new Storage({
    keyFilename: path.join(__dirname, "../config/buzzable-da15b0e340ca.json"),
    projectId: 'buzzable',
});


var storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './uploads');
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + '.' + file.mimetype.split('/')[1]);
    }
})
var upload = multer({
    storage: storage
}).any();


router.post('/uploadImageFiles', function(req, res) {

    upload(req, res, function(err) {
        var fileUploadingintoBucket = uploadingFiles(req, res)
        if (fileUploadingintoBucket) {
            res.json({
                'msg': 'Image uploaded successfully!'
            });
            console.log('Image uploaded successfully!')
        } else {
            res.json({
                'error': 'there is some error'
            })
        }
    });
    var i = 0;

    function uploadingFiles(req, res) {
        let count = req.files.length - 1;
        if (count < i) {
            return 1;
        }
        if (req.files[i].path != undefined && req.files[i].path != null && req.body.data != null && JSON.parse(req.body.data) != undefined) {
            fs.readFileSync(req.files[i].path);
            fileData = JSON.parse(req.body.data);
            var newFileName = fileData[i].fileName;
            uploading(req.files[i].path, fileData[i].bucketName, newFileName, '')
        }
        console.log(req.files[i].path, i++);
        return uploadingFiles(req, res);
    }

    function uploading(localFilePath, bucketName, newFileName, options) {
        options = options || {};

        const bucket = gcs.bucket(bucketName);
        const fileName = path.basename(localFilePath);
        const file = bucket.file(fileName);
        bucket.upload(localFilePath, options, function(err, file) {
            if (err) res.send(err)
            file.move(newFileName)
            var delPath = path.join(__dirname, "../../" + localFilePath);
            try {
                fs.unlinkSync(delPath);
            } catch (err) {
                console.error(err);
            }
        })
    }
})


router.post('/uploadVideoFiles', function(req, res) {

    upload(req, res, function(err) {
        var fileUploadingintoBucket = uploadingFiles(req, res)
        if (fileUploadingintoBucket) {
            res.json({
                'msg': 'Video uploaded successfully!'
            });
            console.log('Video uploaded successfully!')
        } else {
            res.json({
                'error': 'there is some error'
            })
        }
    });
    var i = 0;

    function uploadingFiles(req, res) {
        let count = req.files.length - 1;
        if (count < i) {
            return 1;
        }
        if (req.files[i].path != undefined && req.files[i].path != null && req.body.data != null && JSON.parse(req.body.data) != undefined) {
            fs.readFileSync(req.files[i].path);
            fileData = JSON.parse(req.body.data);
            var newFileName = fileData[i].fileName;
            uploading(req.files[i].path, fileData[i].bucketName, newFileName, '')
        }
        console.log(req.files[i].path, i++);
        return uploadingFiles(req, res);
    }

    function uploading(localFilePath, bucketName, newFileName, options) {
        options = options || {};

        const bucket = gcs.bucket(bucketName);
        const fileName = path.basename(localFilePath);
        const file = bucket.file(fileName);
        bucket.upload(localFilePath, options, function(err, file) {
            if (err) res.send(err)
            file.move(newFileName)
            var delPath = path.join(__dirname, "../../" + localFilePath);
            try {
                fs.unlinkSync(delPath);
            } catch (err) {
                console.error(err);
            }
        })
    }
})

router.post('/uploadAudioFiles', function(req, res) {

    upload(req, res, function(err) {
        var fileUploadingintoBucket = uploadingFiles(req, res)
        if (fileUploadingintoBucket) {
            res.json({
                'msg': 'Audio uploaded successfully!'
            });
            console.log('Audio uploaded successfully!')
        } else {
            res.json({
                'error': 'there is some error'
            })
        }
    });
    var i = 0;

    function uploadingFiles(req, res) {
        let count = req.files.length - 1;
        if (count < i) {
            return 1;
        }
        if (req.files[i].path != undefined && req.files[i].path != null && req.body.data != null && JSON.parse(req.body.data) != undefined) {
            fs.readFileSync(req.files[i].path);
            fileData = JSON.parse(req.body.data);
            var newFileName = fileData[i].fileName;
            uploading(req.files[i].path, fileData[i].bucketName, newFileName, '')
        }
        console.log(req.files[i].path, i++);
        return uploadingFiles(req, res);
    }

    function uploading(localFilePath, bucketName, newFileName, options) {
        options = options || {};

        const bucket = gcs.bucket(bucketName);
        const fileName = path.basename(localFilePath);
        const file = bucket.file(fileName);
        bucket.upload(localFilePath, options, function(err, file) {
            if (err) res.send(err)
            file.move(newFileName)
            var delPath = path.join(__dirname, "../../" + localFilePath);
            try {
                fs.unlinkSync(delPath);
            } catch (err) {
                console.error(err);
            }
        })
    }
})



router.post('/getData',function(req,res){
    console.log("req.body ",req.body)
    var query="SELECT a.*,b.* from media_item as a,responses as b where b.matter_id='"+req.body.id+"' and a.response_media_id=b.id"
    // "SELECT a.*,b.* from media_item as a,responses as b where b.matter_id=10 and a.response_media_id=b.id"
	 pool.query(query, (error, results) => {
	 	if (error) {
		   throw error
		   console.log(error)
	 	}
	 	 //response.status(200).json(results.rows)
		//  console.log(results.rows)
        //  res.send(results.rows)
         res.json({"data":results.rows,"matter":req.body,"status": 200, "textData": 'data'});
	   })
	

})

module.exports = router