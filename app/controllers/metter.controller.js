"use strict";

var models = require('../models');

module.exports = {
  
    // GET metter heading and discription 
    GET: function(req, res) {
	   
		models.Metter.findAll({
			attributes: ['id','matter_name','matter_description','matter_url','unique_string','company_name','expiry_date'],
		})
		.then(function(result){
            res.json({data:result,status: 200, textData: 'data'});
        })
    },

    delete: function(req, res) {
		 console.log(req.body)
		models.Metter.destroy({
			where: {
				 id : req.body.id
			}
		})
		.then(function(result){
            models.Metter.findAll({
				attributes: ['id','matter_name','matter_description','matter_url','unique_string','company_name','expiry_date'],
			})
			.then(function(result){
				res.json({data:result,status: 200, textData: 'data'});
			})  
        })
	},

	get_edit: function(req, res) {
		 models.Metter.find({
			attributes: ['id','matter_name','matter_description','matter_url','unique_string','company_name','expiry_date'],
			where: {
				 id : req.body.id
			}
		})
		.then(function(result){
            res.json({data:result,status: 200, textData: 'data'});
        })
	},   
	
	metter_post_edit_request: function(req, res) {
		console.log("req.body.company_nameeeeeeee ",req.body.company_name)
		models.Metter.update({
		   matter_name:req.body.heading,
		   matter_description:req.body.discription,
		   company_name:req.body.company_name,
		   expiry_date:req.body.expiry_date
	   },{
		   where: {
			   id : req.body.id
		   }
	   })	
	   .then(function(result){
		   
			models.Metter.findAll({
		   attributes: ['id','matter_name','matter_description','matter_url','unique_string','company_name','expiry_date'],
		   })
		   .then(function(result){
				  res.json({data:result,status: 200, textData: 'data'});
			   })
		})
   }
	
}
