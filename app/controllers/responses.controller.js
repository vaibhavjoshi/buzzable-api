"use strict";

var models = require('../models');

module.exports = {

     // POST Email response
    POST: function(req, res) {
	   models.Responses.create({
			matter_id : req.body.matter_id,
			email_id:   req.body.email_id,
			device_id: req.body.device_id
		 })
		 .then(function(result){
		   res.json({data:result,status: 200, textData: 'data'})
		 }) 
	 },

		 get_selected_response:  function(req, res) {
			models.Media_item.belongsTo(models.Responses, {targetKey:'id',foreignKey: 'response_media_id'});
			models.Media_item.findAll({
				include: [{
				  model: models.Responses,
				  where: {matter_id: req.body.id,},
				  attributes: ['device_id','email_id']
				 }]
			  }).then(function(result){
				res.json({data:result,matter: req.body,status: 200, textData: 'data'});
				 //res.send(datavalue);
			})
		  }

	
   	
}



