'use strict';

var fs = require('fs');
var path = require('path');
var Sequelize = require('sequelize');
var config = require(path.join(__dirname, '..', 'config', 'config.js'));
var sequelize = new Sequelize(config.database, config.username, config.password, config)
// Chain in Sequelize-Values NPM
require('sequelize-values')(Sequelize)

sequelize.authenticate()
            .then(() => {
                    console.log("connection has been established successfully");
            })
            .catch((err)=>{
                    console.log("Error while connecting database :", err);
            })

var db = {}

db.sequelize = sequelize;
db.Sequelize = Sequelize;

// Extract Models from files and run sequelize.import for each
try{
fs.readdirSync(__dirname)
    .filter(function (file) {
        return (file.indexOf('.') !== 0) && (file !== 'index.js')
    })
    .forEach(function (file) {
        var model = sequelize.import(path.join(__dirname, file))
        db[model.name] = model;
    })
  

// Dynamic Relations extracted from classMethods on each model file
Object
    .keys(db)
    .forEach(function (modelName) {
        if ('associate' in db[modelName]) {
            db[modelName].associate(db)
        }
    });
}catch(error){
     console.log(error)
}    

module.exports = db
