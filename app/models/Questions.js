'use strict';
module.exports = function(sequelize, DataTypes) {
  var Questions = sequelize.define('Questions', {
    // id : true,
    question : DataTypes.TEXT,
    createdAt:DataTypes.DATE,
    updatedAt:DataTypes.DATE,
    deleted_at:DataTypes.BIGINT,
  }, {
	tableName: 'questions', 
    classMethods: {
      associate: function(models) {
		  // associations can be defined here
      }
    }
  })
  return Questions;
}
