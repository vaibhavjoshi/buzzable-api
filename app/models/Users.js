'use strict';
module.exports = function(sequelize, DataTypes) {
  var Users = sequelize.define('Users', {
    tenant_id: DataTypes.INTEGER,
    username: DataTypes.CHAR,
    password: DataTypes.CHAR,
    user_type: DataTypes.CHAR,
    email_id: DataTypes.CHAR,
    first_name: DataTypes.CHAR,
    last_name: DataTypes.CHAR,
    last_login: DataTypes.DATE,
    created_at: DataTypes.DATE,
    updated_at: DataTypes.INTEGER,
    active_status: DataTypes.BOOLEAN, 
  }, {
	tableName: 'Users', 
    classMethods: {
      associate: function(models) {
		  // associations can be defined here
      }
    }
  })
  return Users;
}
