'use strict';

module.exports = function(sequelize, DataTypes) {
  var Media_item = sequelize.define('Media_item', {
      id: { type : DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
      response_media_id: DataTypes.BIGINT,
      media_type: DataTypes.CHAR,
      media_path: DataTypes.CHAR,
      media_description: DataTypes.CHAR,
      media_size: DataTypes.DATE,
      created_at: DataTypes.INTEGER,
      updated_at: DataTypes.INTEGER,
 }, {
	  tableName: 'media_item', 
    classMethods: {
      associate: function(models) {
		  }
    }
  })
  return Media_item
}
