'use strict';
module.exports = function(sequelize, DataTypes) {
  var Responses = sequelize.define('Responses', {
    id: { type : DataTypes.INTEGER, primaryKey: true,autoIncrement: true },
    matter_id: DataTypes.INTEGER,
    device_id: DataTypes.CHAR,
    email_id: DataTypes.CHAR,
    submitted_at: DataTypes.DATE,
    created_at: DataTypes.INTEGER,
    updated_at: DataTypes.INTEGER,
  }, {
	tableName: 'responses', 
    classMethods: {
      associate: function(models) {
		  // associations can be defined here
      }
    }
  })
  return Responses;
}
