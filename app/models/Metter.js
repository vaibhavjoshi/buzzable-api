'use strict';
module.exports = function(sequelize, DataTypes) {
  var Metter = sequelize.define('Metter', {
    id: { type : DataTypes.INTEGER, primaryKey: true,autoIncrement: true },
    user_id: DataTypes.BIGINT,
    tenant_id: DataTypes.BIGINT,
    matter_name: DataTypes.CHAR,
    matter_description:  DataTypes.CHAR,
    matter_category: DataTypes.CHAR,
    matter_url:  DataTypes.CHAR,
    unique_string:  DataTypes.CHAR,
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
    expiry_date: DataTypes.DATE, 
    archived :  DataTypes.BOOLEAN,
    company_name: DataTypes.CHAR, 
  }, {
	tableName: 'matter', 
    classMethods: {
      associate: function(models) {
		  // associations can be defined here
      }
    }
  })
  return Metter;
}
