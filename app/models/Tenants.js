'use strict';
module.exports = function(sequelize, DataTypes) {
  var Tenants = sequelize.define('Tenants', {
    id: { type : DataTypes.INTEGER, primaryKey: true },
    account_name: DataTypes.CHAR,
    contact_email: DataTypes.CHAR,
    contact_phone: DataTypes.CHAR,
    contact_city: DataTypes.CHAR,
    contact_state: DataTypes.CHAR,
    conatct_zip: DataTypes.CHAR,
    contact_country: DataTypes.CHAR,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    active_status: DataTypes.BOOLEAN, 
  }, {
	tableName: 'Tenants', 
    classMethods: {
      associate: function(models) {
		  // associations can be defined here
      }
    }
  })
  return Tenants;
}
