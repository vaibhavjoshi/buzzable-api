'use strict';
module.exports = function(sequelize, DataTypes) {
  var Response_media = sequelize.define('Response_media', {
    id: { type : DataTypes.INTEGER, primaryKey: true },
    response_id: DataTypes.BIGINT,
    response_sentiment: DataTypes.CHAR,
    response_category: DataTypes.CHAR,
    response_relevance: DataTypes.CHAR,
    response_review_required: DataTypes.BOOLEAN,
    created_at: DataTypes.INTEGER,
    updated_at: DataTypes.INTEGER,
    deleted_at: DataTypes.BIGINT, 
   }, {
	tableName: 'matterresponse_media', 
    classMethods: {
      associate: function(models) {
		  // associations can be defined here
      }
    }
  })
  return Response_media;
}
