var express = require('express');
var router = express.Router();
var get_metter = require('../controllers/metter.controller.js')
var delete_metter = require('../controllers/metter.controller.js')
var get_question = require('../controllers/question.controllers.js')
var post_response = require('../controllers/media_item.controller.js')
var post_response_id = require('../controllers/responses.controller.js')
var metter_post = require('../controllers/metter_post.controller.js')
var mails = require('../controllers/mail.controller.js')
var get_edit_matter = require('../controllers/metter.controller.js')
var metter_post_edit_request = require('../controllers/metter.controller.js')
var media = require('../controllers/media.controller.js');

router.use('/api',media)



router.get('/api/get_metter', get_metter.GET)
router.post('/api/get_question', get_question.POST)
router.post('/api/post_response', post_response.POST)
router.post('/api/post_response_id', post_response_id.POST) 
router.post('/api/metter_post', metter_post.POST)
router.post('/api/delete_metter', delete_metter.delete) 
router.post('/api/mail',mails.mail)
router.post('/api/get_edit_metter', get_edit_matter.get_edit)
router.post('/api/metter_post_edit_request', metter_post_edit_request.metter_post_edit_request)
router.post('/api/get_selected_response',post_response_id.get_selected_response)


// router.get('/', function(req, res) {
//     console.log("default rout called")
//     res.send("successfully hit on router.js")
// })

module.exports = router